package org.videolan.podcast;

import android.app.Application;

public class PodcastApp extends Application {

    PodcastService mService;

    public PodcastService getService() {
        return mService;
    }

    public void setService(PodcastService mService) {
        this.mService = mService;
    }
}

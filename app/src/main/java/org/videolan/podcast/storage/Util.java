package org.videolan.podcast.storage;

import java.io.Closeable;
import java.io.IOException;

public class Util {

    public static void closeNicely(Closeable closeable) {
        if (closeable == null)
            return;
        try {
            closeable.close();
        } catch (IOException e) {}
    }
}

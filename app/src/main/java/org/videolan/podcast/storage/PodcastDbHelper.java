package org.videolan.podcast.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class PodcastDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Podcast.db";

    public PodcastDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static abstract class PodcastEntry implements BaseColumns {
        public static final String TABLE_NAME = "podcast";
        public static final String COLUMN_NAME_URL = "url";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_AUTHOR = "author";
        public static final String COLUMN_NAME_IMAGE_LINK = "imagelink";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PodcastEntry.TABLE_NAME + " (" +
                    PodcastEntry._ID + " INTEGER PRIMARY KEY," +
                    PodcastEntry.COLUMN_NAME_URL + TEXT_TYPE + COMMA_SEP +
                    PodcastEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    PodcastEntry.COLUMN_NAME_AUTHOR + TEXT_TYPE + COMMA_SEP +
                    PodcastEntry.COLUMN_NAME_IMAGE_LINK + TEXT_TYPE +  " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PodcastEntry.TABLE_NAME;

    public static final String[] PROJECTION = {
            PodcastEntry._ID,
            PodcastEntry.COLUMN_NAME_URL,
            PodcastEntry.COLUMN_NAME_TITLE,
            PodcastEntry.COLUMN_NAME_AUTHOR,
            PodcastEntry.COLUMN_NAME_IMAGE_LINK
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}

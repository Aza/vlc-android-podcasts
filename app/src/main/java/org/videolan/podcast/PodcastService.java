package org.videolan.podcast;

import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Enclosure;
import com.einmalfel.earl.Feed;
import com.einmalfel.earl.Item;

import org.videolan.podcast.storage.PodcastDbHelper;
import org.videolan.podcast.storage.Util;
import org.videolan.vlc.extensions.api.VLCExtensionItem;
import org.videolan.vlc.extensions.api.VLCExtensionService;

import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PodcastService extends VLCExtensionService {
    private static final String TAG = "PodcastService";
    PodcastDbHelper mDbHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        mDbHelper = new PodcastDbHelper(this);
    }

    @Override
    protected void browse(final String stringId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                InputStream inputStream = null;
                try {
                    DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
                    inputStream = new URL(stringId).openConnection().getInputStream();
                    final Feed feed = EarlParser.parseOrThrow(inputStream, 0);
                    final ArrayList<VLCExtensionItem> casts = new ArrayList<>();
                    VLCExtensionItem vlcItem;
                    for (Item item : feed.getItems()) {
                        String dateDisplay = null;

                        if (item.getPublicationDate() != null) {
                             dateDisplay = dateFormat.format(item.getPublicationDate());
                         }
                        List<? extends Enclosure> enclosures = item.getEnclosures();
                        String link = item.getLink();
                        int type = VLCExtensionItem.TYPE_AUDIO;
                        for (Enclosure enclosure : enclosures) {
                            if (enclosure.getType().startsWith("audio")) {
                                link = enclosure.getLink();
                                type = VLCExtensionItem.TYPE_AUDIO;
                                break;
                            } else if (enclosure.getType().startsWith("video")) {
                                link = enclosure.getLink();
                                type = VLCExtensionItem.TYPE_VIDEO;
                                break;
                            } else
                                continue;
                        }

                        //format link
                        if (link.contains("?"))
                            link = link.replace("?", "").split("dest")[0];

                         vlcItem = new VLCExtensionItem()
                                .setLink(link)
                                .setTitle(item.getTitle())
                                .setType(type);
                         if (!TextUtils.isEmpty(dateDisplay))
                             vlcItem.setSubTitle(dateDisplay);
                         if (!TextUtils.isEmpty(item.getImageLink()))


                            vlcItem.setImageUri(Uri.parse(item.getImageLink()));
                        casts.add(vlcItem);
                    }
                    mServiceHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updateList(feed.getTitle(), casts, false, false);
                        }
                    });
                } catch (Exception e) {}
                finally {
                    Util.closeNicely(inputStream);
                }
            }
        }).start();
    }

    @Override
    protected void refresh() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Refresh");
        // TODO
    }

    @Override
    public void onInitialize() {
        browseSubscriptions();
    }

    private void browseSubscriptions() {
        Cursor cursor = mDbHelper.getReadableDatabase().rawQuery("select * from " + PodcastDbHelper.PodcastEntry.TABLE_NAME, null);
        final LinkedList<VLCExtensionItem> items = new LinkedList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String url = cursor.getString(cursor
                        .getColumnIndex(PodcastDbHelper.PodcastEntry.COLUMN_NAME_URL));
                String title = cursor.getString(cursor
                        .getColumnIndex(PodcastDbHelper.PodcastEntry.COLUMN_NAME_TITLE));
                String author = cursor.getString(cursor
                        .getColumnIndex(PodcastDbHelper.PodcastEntry.COLUMN_NAME_AUTHOR));
                String imageLink = cursor.getString(cursor
                        .getColumnIndex(PodcastDbHelper.PodcastEntry.COLUMN_NAME_IMAGE_LINK));
                VLCExtensionItem vlcItem = new VLCExtensionItem(url)
                        .setLink(url)
                        .setTitle(title)
                        .setSubTitle(author)
                        .setType(VLCExtensionItem.TYPE_DIRECTORY);
                        if (!TextUtils.isEmpty(imageLink))
                            vlcItem.setImageUri(Uri.parse(imageLink));
                items.add(vlcItem);
                cursor.moveToNext();
            }
            cursor.close();
        }
            mServiceHandler.post(new Runnable() {
                @Override
                public void run() {
                    updateList("podcasts", items, true, false);
                }
            });
    }
}

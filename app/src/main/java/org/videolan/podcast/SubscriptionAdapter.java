package org.videolan.podcast;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.videolan.vlc.extensions.api.VLCExtensionItem;

import java.util.ArrayList;
import java.util.Collection;

import androidx.recyclerview.widget.RecyclerView;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ViewHolder> implements View.OnClickListener {

    private static final int TAG_POSITION = R.id.title;
    private ArrayList<VLCExtensionItem> mItems = new ArrayList<>();
    MainActivity mActivity;

    public SubscriptionAdapter(MainActivity mainActivity) {
        mActivity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VLCExtensionItem item = mItems.get(position);
        holder.title.setText(item.title);
        holder.author.setText(item.subTitle);
        Picasso.with(holder.itemView.getContext()).load(item.getImageUri()).into(holder.image);
        holder.itemView.setTag(TAG_POSITION, position);
        holder.itemView.setOnClickListener(this);
    }

    public ArrayList<VLCExtensionItem> getItems(){
        return mItems;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void remove(VLCExtensionItem item){
        mItems.remove(item);
    }

    public void clear() {
        mItems.clear();
    }

    public void addAll(Collection items){
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        int position = ((Integer)v.getTag(TAG_POSITION)).intValue();
        mActivity.removePodcastDialog(mItems.get(position));
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, author;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            author = (TextView) itemView.findViewById(R.id.author);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}

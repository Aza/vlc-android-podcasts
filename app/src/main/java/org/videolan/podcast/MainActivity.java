package org.videolan.podcast;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Feed;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.videolan.podcast.storage.PodcastDbHelper;
import org.videolan.podcast.storage.Util;
import org.videolan.vlc.extensions.api.PermissionHelper;
import org.videolan.vlc.extensions.api.PermissionStatus;
import org.videolan.vlc.extensions.api.VLCExtensionItem;
import org.videolan.vlc.extensions.api.VLCPermission;
import org.videolan.vlc.extensions.api.VLCPermissionRequest;
import org.videolan.vlc.extensions.api.tools.Helpers;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.collection.SimpleArrayMap;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static org.videolan.podcast.storage.PodcastDbHelper.PodcastEntry;

public class MainActivity extends AppCompatActivity implements PermissionHelper.OnVLCPermissionResult {

    private static final String TAG = "MainActivity";
    private AlertDialog mAlertDialog;
    PodcastDbHelper mDbHelper;
    SubscriptionAdapter mAdapter;

    TextView mTitle;
    RecyclerView mList;
    private RecyclerView.LayoutManager mLayoutManager;
    private SimpleArrayMap<String, List<Feed>> feedsMap = new SimpleArrayMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Helpers.checkVlc(this)) {
            finish();
            return;
        }
        mDbHelper = new PodcastDbHelper(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = (TextView) findViewById(R.id.main_title);
        mList = (RecyclerView) findViewById(R.id.subscriptions_list);
        mLayoutManager = new LinearLayoutManager(this);
        mList.setLayoutManager(mLayoutManager);
        mAdapter = new SubscriptionAdapter(this);
        mList.setAdapter(mAdapter);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddPodcastDialog();
            }
        });

        VLCPermissionRequest readPermission = new VLCPermissionRequest();
         readPermission.setPermission(VLCPermission.READ_STATUS);
         readPermission.setReason("The read status is needed to avoid showing already read episodes");

        VLCPermissionRequest progressPermission = new VLCPermissionRequest();
        progressPermission.setPermission(VLCPermission.PROGRESS_STATUS);
        progressPermission.setReason("The progress is used to display a progress bar in the app");
        final PermissionHelper permissionHelper = PermissionHelper.newInstance(this);
        permissionHelper.askPermissions(readPermission, progressPermission);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAlertDialog != null && mAlertDialog.isShowing())
            mAlertDialog.dismiss();
    }

    private void updateList() {
        Cursor cursor = mDbHelper.getReadableDatabase().rawQuery("select * from " + PodcastEntry.TABLE_NAME, null);
        LinkedList<VLCExtensionItem> items = new LinkedList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String url = cursor.getString(cursor
                        .getColumnIndex(PodcastEntry.COLUMN_NAME_URL));
                String title = cursor.getString(cursor
                        .getColumnIndex(PodcastEntry.COLUMN_NAME_TITLE));
                String author = cursor.getString(cursor
                        .getColumnIndex(PodcastEntry.COLUMN_NAME_AUTHOR));
                String uriString = cursor.getString(cursor
                        .getColumnIndex(PodcastEntry.COLUMN_NAME_IMAGE_LINK));
                VLCExtensionItem item = new VLCExtensionItem(url)
                        .setLink(url)
                        .setTitle(title)
                        .setSubTitle(author)
                        .setType(VLCExtensionItem.TYPE_DIRECTORY);
                if (!TextUtils.isEmpty(uriString))
                    item.setImageUri(Uri.parse(uriString));
                items.add(item);
                cursor.moveToNext();
            }
            cursor.close();
        }
        mAdapter.addAll(items);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent mActivity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear) {
            removeAllPodcastDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    public void removePodcastDialog(final VLCExtensionItem item) {
        final Context context = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.remove_dialog_title);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                return;
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        removePodcast(item);
                        mAdapter.remove(item);
                        mHandler.obtainMessage(PODCAST_REMOVE_success).sendToTarget();
                    }
                }).start();
            }
        });
        mAlertDialog = builder.show();
    }

    public void removeAllPodcastDialog() {
        final Context context = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.remove_all_dialog_title);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                return;
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (VLCExtensionItem item : mAdapter.getItems())
                            removePodcast(item);
                        mAdapter.clear();
                        mHandler.obtainMessage(PODCAST_REMOVE_ALL_success).sendToTarget();
                    }
                }).start();
            }
        });
        mAlertDialog = builder.show();
    }

    public void removePodcast(VLCExtensionItem item) {
        String url = item.link;
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String selection = PodcastEntry.COLUMN_NAME_URL + " = ?";
        String[] selectionArgs = {url};
        db.delete(PodcastEntry.TABLE_NAME, selection, selectionArgs);
    }


    public void showAddPodcastDialog() {
        final Context context = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AppCompatEditText input = new AppCompatEditText(context);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        builder.setTitle("Add a new podcast");
        builder.setMessage("write or paste here your podcast address");
        builder.setView(input);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                return;
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addPodcast(input);
            }
        });
        mAlertDialog = builder.show();
    }

    private void addPodcast(final AppCompatEditText input) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String link = input.getText().toString().trim();

                    for (VLCExtensionItem item : mAdapter.getItems())
                        if (link.equals(item.getLink())) {
                            mHandler.obtainMessage(PODCAST_ALREADY_ADDED).sendToTarget();
                            return;
                        }

                    InputStream inputStream = new URL(link).openConnection().getInputStream();
                    Feed feed = EarlParser.parseOrThrow(inputStream, 0);

                    SQLiteDatabase db = mDbHelper.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(PodcastEntry.COLUMN_NAME_URL, link);
                    values.put(PodcastEntry.COLUMN_NAME_TITLE, feed.getTitle());
                    values.put(PodcastEntry.COLUMN_NAME_AUTHOR, feed.getAuthor());
                    values.put(PodcastEntry.COLUMN_NAME_IMAGE_LINK, feed.getImageLink());

                    db.insert(PodcastEntry.TABLE_NAME, null, values);
                    mHandler.obtainMessage(PODCAST_ADD_success).sendToTarget();
                } catch (Exception e) {
                    mHandler.obtainMessage(PODCAST_ADD_FAIL, e).sendToTarget();
                }
            }

            private byte[] getImage(String imageLink) {
                BufferedInputStream bis = null;
                ByteArrayOutputStream baf = null;
                try {
                    URL imageUrl = new URL(imageLink);
                    URLConnection ucon = imageUrl.openConnection();
                    bis = new BufferedInputStream(ucon.getInputStream());

                    byte[] buffer = new byte[1024];
                    baf = new ByteArrayOutputStream(1024);
                    int current;
                    while ((current = bis.read(buffer, 0, 1024)) > 0) {
                        baf.write(current);
                    }

                    return baf.toByteArray();
                } catch (Exception e) {
                    Log.d(TAG, "Error: " + e.toString());
                } finally {
                    Util.closeNicely(bis);
                    Util.closeNicely(baf);
                }
                return null;
            }
        }).start();
    }

    private static final int PODCAST_ADD_FAIL = 0;
    private static final int PODCAST_ADD_success = 1;
    private static final int PODCAST_REMOVE_success = 2;
    private static final int PODCAST_REMOVE_ALL_success = 3;
    private static final int PODCAST_ALREADY_ADDED = 4;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PODCAST_ADD_FAIL:
                    Snackbar.make(getWindow().getDecorView(), ((Exception) msg.obj).getMessage(), Snackbar.LENGTH_LONG).show();
                    break;
                case PODCAST_ADD_success:
                    updateList();
                    break;
                case PODCAST_REMOVE_success:
                    Snackbar.make(getWindow().getDecorView(), R.string.removed_snackbar_title, Snackbar.LENGTH_SHORT).show();
                    mAdapter.notifyDataSetChanged();
                    break;
                case PODCAST_REMOVE_ALL_success:
                    Snackbar.make(getWindow().getDecorView(), R.string.removed_all_snackbar_title, Snackbar.LENGTH_SHORT).show();
                    mAdapter.notifyDataSetChanged();
                    break;
                case PODCAST_ALREADY_ADDED:
                    Snackbar.make(getWindow().getDecorView(), R.string.already_added_snackbar_title, Snackbar.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    @Override
    public void onPermissionResult(VLCPermission permission, PermissionStatus status) {
        if (status == PermissionStatus.GRANTED) {
//            Snackbar.make(mTitle, permission.getKey()+" granted", Snackbar.LENGTH_LONG).show();
        } else if (status == PermissionStatus.DENIED_MANIFEST) {
//            Snackbar.make(mTitle, permission.getKey()+" no permission in manifest", Snackbar.LENGTH_LONG).show();
        }else if (status == PermissionStatus.DENIED) {
//            Snackbar.make(mTitle, permission.getKey()+" denied", Snackbar.LENGTH_LONG).show();
        } else {
//            Snackbar.make(mTitle, permission.getKey()+" unknown", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onVLCNotInstalled() {
        Snackbar.make(mTitle, "VLC is not installed!", Snackbar.LENGTH_LONG).show();
    }
}
